var express = require('express');
var app = express();
var fs = require('fs');

var config = null;
if (fs.existsSync('config.json')) {
    var fileContent = fs.readFileSync('config.json', 'utf8');
    config = JSON.parse(fileContent);
} else {
    config = {
        port: '3001',
        client_origin: '*'
    }
}


//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', config.client_origin);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(allowCrossDomain);

var token = 'AER70MR';

app.post('/login', function (req, res) {

    if (req.body.username === 'abc' && req.body.password === '123') {
        var responseObj = {
          login_status: 0,
          role: "tehnical",
          token: token
        };
    }
    else if (req.body.username === 'hrm' && req.body.password === '123') {
        var responseObj = {
          login_status: 0,
          role: "hr",
          token: token
        };
    }
     else if (req.body.username === 'super' && req.body.password === '123') {
        var responseObj = {
          login_status: 0,
          role: "supervisor",
          token: token
        };
    }
     else if (req.body.username === 'admin' && req.body.password === '123') {
        var responseObj = {
          login_status: 0,
          role: "admin",
          token: token
        };
    }
     else {
        var responseObj = {
          login_status: 1
        };
    }
    res.send(responseObj);
});

app.post('/logout', function (req, res) {
    res.send({status: 'OK'});
});

app.listen(config.port, function () {
    console.log('Example app listening on port ' + config.port);
});