define([

    'Path',
    'routers/MenuX',
    'routers/ParentPaneX',
    'routers/LoginX',
    'routers/CandidatesX',
    'routers/DetailsX',
    'routers/ForgotX',
    'routers/RegisterX',
    'routers/HomeX',
    'routers/CalendarX',
    'routers/AccountDetailsX',
    'routers/UploadPhotosX',
    'routers/ProfileX',
    'routers/EventsX',
    'routers/CreateEventX',
    'routers/FeaturedX',

], function (

    Path,
    MenuMixin,
    LoginMixin,
    CandidatesMixin,
    DetailsMixin,
    RegisterMixin,
    AccountDetailsMixin,
    ForgotMixin,
    HomeMixin,
    CalendarMixin,
    ParentPaneMixin,
    UploadPhotosPaneMixin,
    ProfileMixin,
    EventsMixin,
    CreateEventMixin,
    FeaturedMixin,
) {

        return Backbone.Router.extend(_.extend({},
            MenuMixin,
            LoginMixin,
            CandidatesMixin,
            DetailsMixin,
            ForgotMixin,
            RegisterMixin,
            HomeMixin,
            CalendarMixin,
            AccountDetailsMixin,
            ParentPaneMixin,
            UploadPhotosPaneMixin,
            ProfileMixin,
            EventsMixin,
            CreateEventMixin,
            FeaturedMixin,
            {

                layoutStates: null,

                routes: {
                    'login': 'routeTo_login',
                    'candidates/selectedtab=:selectedtab': 'routeTo_candidates',
                    'details': 'routeTo_details',
                    'forgot': 'routeTo_forgot',
                    'register': 'routeTo_register',
                    'account_details': 'routeTo_account_details',
                    'home': 'routeTo_home',
                    'calendar': 'routeTo_calendar',
                    'uploadPhotos': 'routeTo_uploadPhotos',
                    'profile/photograph=:id': 'routeTo_profile',
                    'events':'routeTo_events',
                    'createEvent':'routeTo_createEvent',
                    'featured':'routeTo_featured',

                    '*defaultPath': 'routeTo_defaultPath',
                },
                routesHit: [],

                back: function () {
                    if (this.routesHit.length > 0) {
                        var routeTo = this.routesHit[this.routesHit.length - 2];
                        delete this.routesHit[this.routesHit, length - 1];
                        this.navigate(routeTo, { trigger: true, replace: true });
                    } else {
                        this.navigate('/', { trigger: true, replace: true });
                    }
                },

                initialize: function () {
                    var self = this;
                    var authenticationToken = null;

                    this.path = new Path({
                        router: this,
                    });

                    this.setAuthenticationToken = function () {
                        authenticationToken = this.getCookie("authenticated");

                    };

                    this.isAuthenticated = function () {
                        if (authenticationToken === null) {
                            return false;
                        } else {
                            return true;
                        }
                    };

                    this.getRole = function () {
                        role = this.getCookie("role");
                        return role;
                    };

                    this.getAuthenticationToken = function () {
                        authenticationToken = this.getCookie("authenticated");
                        return authenticationToken;
                    };

                    this.setAuthenticationToken();
                    this.login_initialize();
                    this.menu_initialize();
                    this.candidates_initialize();
                    this.details_initialize();
                    this.parentPane_initialize();
                    this.forgot_initialize();
                    this.home_initialize();
                    this.calendar_initialize();
                    this.register_initialize();
                    this.profile_initialize();
                    this.uploadPhotos_initialize();
                    this.accountDetails_initialize();
                    this.events_initialize();
                    this.createEvent_initialize();
                    this.featured_initialize();
                    this.candidatesPaneModel.loadCandidates();

                    var router = this;
                    this.routesHit = [];

                    Backbone.history.start({});

                },

                routeTo_details: function () {
                    this.parentPane_toggle(SECTIONS.candidates);
                },
                routeTo_createEvent: function(){
                    this.parentPane_toggle(SECTIONS.createEvent);
                },
                routeTo_login: function () {
                    this.parentPane_toggle(SECTIONS.login);
                },
                routeTo_register: function () {
                    this.parentPane_toggle(SECTIONS.register);
                },
                routeTo_account_details: function () {
                    this.parentPane_toggle(SECTIONS.account_details);
                },
                routeTo_forgot: function () {
                    this.parentPane_toggle(SECTIONS.forgot);
                },
                routeTo_featured: function(){
                    this.parentPane_toggle(SECTIONS.featured);
                },
                routeTo_home: function () {
                    this.parentPane_toggle(SECTIONS.home);
                },
                routeTo_events:function()
                {
                    this.parentPane_toggle(SECTIONS.events);
                },
                routeTo_uploadPhotos: function () {
                    this.parentPane_toggle(SECTIONS.uploadPhotos);
                },
                routeTo_calendar: function () {
                    this.parentPane_toggle(SECTIONS.calendar);
                },
                routeTo_profile: function (id) {
                    this.profilePaneModel.set({ id: id });
                    this.parentPane_toggle(SECTIONS.profile);
                    this.path.setID(id);

                },
                routeTo_candidates: function (selectedtab) {
                    this.candidatesPaneModel.loadCandidates();
                    var defaults = this.parseSelectedTab(selectedtab);
                    this.candidatesPaneModel.set({ selectedTab: defaults });
                    this.parentPane_toggle(SECTIONS.candidates);
                },

                routeTo_defaultPath: function () {
                    this.parentPane_toggle(SECTIONS.login);
                },
                parseSelectedTab: function (selectedtab) {
                    switch (selectedtab) {
                        case 'evaluated':
                        case 'notEvaluated':
                            {
                                return selectedtab;
                            }
                        default:
                            {
                                return 'notEvaluated';
                            }
                    }

                }
            }));

    });