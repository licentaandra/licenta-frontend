require.config({
    baseUrl: 'js',
    paths: {
        'jquery': 'lib/jquery',
        'lodash': 'lib/lodash',
        'backbone': 'lib/backbone',
        'bootstrap': 'lib/bootstrap',
        'fullcalendar': 'lib/fullcalendar.min',
        'moment': 'lib/moment.min',
        'material': 'lib/material.min',
        'magnificPopup':'lib/jquery.magnific-popup.min',
        'lightbox':'lib/lightbox-plus-jquery.min',
        'Chart':'lib/Chart.min',
        
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'lodash': {
            exports: '_'
        },
        'backbone': {
            deps: ['lodash', 'jquery'],
            exports: 'Backbone'
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'moment': {
            deps: ['jquery']
        },
        'fullcalendar': {
            deps: ['jquery', 'moment']
        },
        'Chart':{
            deps: ['jquery']
        },
        'material': {
            deps: ['jquery', 'moment']
        },
        'magnificPopup':{
             deps: ['jquery', 'moment']
        },
        'lightbox': {
            deps: ['jquery']
        },
        'Router': ['jquery', 'backbone', 'bootstrap']
    }
});

require(['Router'], function (Router) {
    new Router();
});

window.SECTIONS = {
    candidates: 'candidates',
    login: 'login',
    details: 'details',
    forgot: 'forgot',
    register: 'register',
    account_details: 'account_details',
    home: 'home',
    calendar: 'calendar',
    uploadPhotos: 'uploadPhotos',
    profile: 'profile',
    events:'events',
    createEvent:'createEvent',
    featured:'featured'

};

window.API_DOMAIN = 'http://localhost:8080/';

var PTypes = [
    { name: 'Advertising Photography', icon: 'fa-shopping-cart',id:0 },
    { name: 'Fashion Photography', icon: 'fa-shopping-bag',id:1 },
    { name: 'Food Photography', icon: 'fa-cutlery',id:2 },
    { name: 'Wedding Photography', icon: 'fa-heart',id:3 },
    { name: 'Travel Photography', icon: 'fa-map-o',id:4 },
    { name: 'Underwater Photography', icon: 'fa-tint',id:5 },
    { name: 'Macro Photography', icon: 'fa-bug',id:6 },
    { name: 'Architectural Photography', icon: 'fa-university',id:7 },
    { name: 'New Born Photography', icon: 'fa-child',id:8 },
    { name: 'Photojournalism', icon: 'fa-newspaper-o',id:9 },
    { name: 'Portraiture', icon: 'fa-user',id:10 },
    { name: 'Landscape Photography', icon: 'fa-picture-o',id:11 },
    { name: 'Nature Photography', icon: 'fa-tree',id:12 },
    { name: 'Video', icon: 'fa-video-camera',id:13 },
    { name: 'Wildlife Photography', icon: 'fa-paw',id:14 },
    { name: 'Concert Photography', icon: 'fa-microphone',id:15 },
    { name: 'Family Photography', icon: 'fa-users',id:16 },
    { name: 'Scientific Photography', icon: 'fa-flask',id:17 },
    { name: 'Time Lapse Photography', icon: 'fa-hourglass-half',id:18 },
    { name: 'Real Estate Photography', icon: 'fa-home',id:19 },
    { name: 'Event Photography', icon: 'fa-graduation-cap',id:20 },
    { name: 'Black and White', icon: ' fa-star-half-o',id:21 },
    { name: 'HDR Photography', icon: 'fa-camera',id:22 },
    { name: 'Sport photography', icon: 'fa-bicycle',id:23 },
   
];

// Countries
var country_arr = new Array("Hong Kong", "Singapore", "Bangkok", "London", "Macau", "Kuala Lumpur", "Shenzhen", "New York City",
    "Antalya", "Paris", "Istanbul", "Rome", "Dubai", "Guangzhou", "Phuket", "Mecca", "Pattaya", "Taipei City", "Prague", "Shanghai", "'Las Vegas", "Miami", "Barcelona", "Moscow",
    "Beijing", "Los Angeles", "Budapest", "Vienna", "Amsterdam", "Sofia", "Madrid", "Orlando", "Ho Chi Minh City", "Lima", "Berlin", "Tokyo", "Warsaw",
    "Chennai", "Cairo", "Nairobi", "Hangzhou", "Milan", "San Francisco", "Buenos Aires", "Venice", "Mexico City", "Dublin", "Seoul", "Mugla", "Mumbai", "Denpasar", "Delhi", "Toronto", "Zhuhai", "St Petersburg", "Burgas", "Sydney", "Djerba", "Munich", "Cancun",
    "Bucharest", "Punta Cana", "Brussels", "Nice", "Chiang Mai", "Sharm el-Sheikh", "Lisbon", "Marrakech", "Jakarta",
    "Vancouver", "Abu Dhabi", "Kiev", "Doha", "Florence", "Rio de Janeiro", "Melbourne", "Washington D.C.",
    "Christchurch", "Frankfurt", "Baku", "Sao Paulo", "Harare", "Kolkata", "Nanjing");

Sugestions =[
    {
        text:'There are no in your area . Wanna ad it to your interests?',
        id:2
    },
    {
        text:'Was the most booked last month . Wanna ad it to your interests?',
        id:3
    }

]

Most = {
  comments:  {
        number:123,
        difference:11
    },
   bookings: {
        number:12,
        difference:-3
    },
    type:{kind:2},
    top:7,
    year:[9, 11, 8, 6, 13, 7, 7, 10, 2, 4, 12, 17],
    top_type:{
        label:["piza", "dadada", "test", "Blog Posts"],
        data:[8, 5, 18, 16],
        total:3456
    }
}

Event_list = [
    {
        description:"Electric Castle is a music festival that takes place in July, on the spectacular Transylvanian domain of the Banffy\
				Castle, near Cluj-Napoca in Romania. The festival is a unique festival experience that combines music, technology\
				and alternative arts. It features many genres of music including rock, indie, hip hop, electronic and reggae, as\
				well as art installations.",
        name:'Electric Castle',
        date:'07.08.2017',
        ev_type:'2',
        start_time:'09:00',
        end_time:'22:00',
        location:'Cluj',
        id:12
    }
]
Events_me =[
    {
        type:1,
        name:'CONCERT EC',
        volunteers:3,
        max_number:10

    },
    {
        type:2,
        name:'Festivall',
        volunteers:5,
        max_number:10

    }
]

var PEvents = {
    number:4,
    notifications:[
       { name:"Andra",
        link:'test_link',
        date:'12/12/2018',
        time:'12PM-16PM',
        text:'incercare',
        location:'Cluj',
       },
       { name:"Dan",
        link:'test_link',
        date:'12/12/2018',
        time:'12PM-16PM',
        text:'incercareDan',
        location:'Cluj',
       },
        { name:"Serban",
        link:'test_link',
        date:'12/12/2018',
        time:'12PM-16PM',
        text:'Hey! I thought we could all get together and grab something to eat to remember old times!',
        location:'Cluj',
       },
    ]
}






COMMENTS_P = [
    {
        name: 'Flora Garcia',
        time: '2 days ago',
        text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.',
        rating:6,
    },
    {
        name: 'Peter Garric',
        time: '20 minutes ago',
        text: 'I like to ride the bike to work, swimming, and working out. I also like reading design magazines, go to museums, and binge watching a good tv show while it’s raining outside.',
        rating:5
    },
    {
        name: 'Flora Garcia',
        time: '12 days ago',
        text: 'The Crime of the Century, Egiptian Mythology 101, The Scarred Wizard, Lord of the Wings, Amongst Gods, The Oracle, A Tale of Air and Water.',
        rating:3,
    },
]

var CLIENTS3 = [
    {
        "id": 1,
        "internship": ["ios"],
        "name": "Flora Garcia",
        "email": "Flora@yahoo.com",
        "evaluated": 0,

    }
]

var DETAILS = {

    "username": "Sophie Alma",
    "internship": "ios",
    "email": "abc@123.com",
    "phone_number": "0767989093",
    "evaluated": 0,
}

var ANSWERS = [
    {
        "question": "What is your Experience (Projects completed, GitHub Repos)?",
        "answer": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


    },
    {
        "question": "iOS Specific Programming Knowledge (Basic understanding of memory, table views, notification center, delegates, blocks. Not detailed but do you at least know what they are or differences between them.)?",
        "answer": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt."
    },
    {
        "question": "Describe yourself.",
        "answer": " At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. "
    },
    {
        "question": "Which technology do you like the most and why?",
        "answer": "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish."
    },
    {
        "question": "Do you have any technologies you wish you've worked with but never got the chance? What are they and why would you like to learn them?",
        "answer": "But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."


    }

]