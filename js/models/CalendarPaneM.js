define([], function() {

    return Backbone.Model.extend({

        defaults: {
             visible: null,
             events:null,
             requests:null,
             history:null,
             createdEvents:null,
             top:null,
             
        },

        initialize: function(options) {
             var This = this;
            var router = options.router;
            this.setData = function(data) {
                this.set(data);
            };
             this.show = function (show) {
                this.setData({
                    visible: show,
                });
                This.loadRequests();
                This.loadCreatedEvents();
                this.loadChart();
            };

            this.loadCreatedEvents =function(){
                var url = API_DOMAIN + 'events?type=self';
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                       This.setData({
                       createdEvents:data

                    });
                    }
                });
            };   

            this.loadChart = function(){
                
                var url = API_DOMAIN + 'stats?type=self';
                var authenticationToken = router.getCookie("authenticated");
                // setTimeout(function () {
                //     var top = Most;
                //     This.setData({
                //         top: top
                //     });
                // }, 100);
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                       This.setData({
                        top:data
                    });
                    }
                });

            };
            this.loadRequests = function(){           
                var url = API_DOMAIN + 'bookings?type=self';
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                       This.setData({
                        requests: data.unconfirmed,
                        events:data.confirmed,
                        history:data.confirmed

                    });
                    }
                });
            };
            this.sendResponse=function(values){
                authenticationToken = router.getCookie("authenticated");
                var url = API_DOMAIN + 'bookings';
                $.ajax({
                    url: url,
                    type: 'PUT',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(values),
                    processData: false,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                        This.loadRequests();

                    },
                    error: function (data) {
                       console.log('error');
                    }
                });
            };
  
        }
    });
});