define([], function () {

    return Backbone.Model.extend({

        defaults: {
            visible: null,
            profile:null,
        },

        initialize: function (options) {
            var This = this;
            var router = options.router;
            this.setData = function (data) {
                this.set(data);
            };
            this.loadSearch = function(region,types,order){
                 var url = API_DOMAIN + 'search?region='+region+'&types='+types+'&sort='+order;
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                       This.setData({
                        profile:data

                    });
                    }
                });
            };
            this.loadProfile = function () {

                var url = API_DOMAIN + 'top';
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                       This.setData({
                        profile:data

                    });
                    }
                });
            }
            this.show = function (show) {
                this.setData({
                    visible: show,
                });
            };

        }

    });

});