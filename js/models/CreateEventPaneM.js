define([], function() {

    return Backbone.Model.extend({

        defaults: {
             visible: null,
        },

        initialize: function(options) {
             var This = this;
            var router = options.router;
            this.setData = function(data) {
                this.set(data);
            };

            this.sendEvent= function(values){
                authenticationToken = router.getCookie("authenticated");
                var url = API_DOMAIN + 'events';
                $.ajax({
                    url: url,
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    mimeType: "multipart/form-data",
                    data: values,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                        console.log('All good');

                    },
                    error: function (data) {
                        console.log('error');
                    }
                });               
            };
             this.show = function (show) {
                this.setData({
                    visible: show,
                });
            };

        }

    });

});