define([], function() {

    return Backbone.Model.extend({

        defaults: {
        },

        initialize: function() {

            this.setData = function(data) {
                this.set(data);
            };

        }

    });

});