define([], function () {

    return Backbone.Model.extend({

        defaults: {
            visible: null,
            username: null,
            password: null,
        },

        initialize: function (options) {
            var router = options.router;
            var This = this;

            var authenticated = router.isAuthenticated();

            this.setData = function (data) {
                this.set(data);
            };


            this.show = function (show) {
                this.setData({
                    visible: show
                });
            };

            this.loadRegister = function (formValues) {
                var url = API_DOMAIN + 'register';
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(formValues),
                    processData: false,
                    success: function (data) {

                        This.loadAuthenticated(formValues);

                    },
                    error: function (data) {
                        $('.alert-error').text("The username or email are already taken").show();
                    }
                });
            };

            this.loadAuthenticated = function (formValues) {
                delete formValues['email'];
                var url = API_DOMAIN + 'login';
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(formValues),
                    processData: false,
                    success: function (data) {
                        router.setCookie("authenticated", 'JWT ' + data.access_token, 30);
                        router.setAuthenticationToken();

                        This.redirectPage(data.access_token);

                    },
                    error: function (data) {
                        $('.alert-error').text("The username or the password is incorrect").show();
                    }
                });
            };
            this.redirectPage = function (token) {

                var url = API_DOMAIN + 'user_details?type=self';
                var authenticationToken = 'JWT ' + token;
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                        console.log('data',data);
                        if (data == null) {
                            // if there is not a role assigned yet to the account , redirect to account details 
                            // because it is the first time when register account information , the state is set to new

                            router.setCookie("role", null, 30);
                            router.accountDetails_enter();
                        }
                        else {
                           
                            router.setCookie("role", data.acc_type_id, 30);
                            router.home_enter();
                            // daca deja are un role selectat , sa mearga direct la home


                        
                        }
                    }
                });

            }

        }

    });

});