define([], function () {

    return Backbone.Model.extend({

        defaults: {
            visible: null,
            details: null,
            answers: null,
            candidateID: null,
            comments: null,
        },

        initialize: function (options) {
            var This = this;
            var router = options.router;

            this.setData = function (data) {
                this.set(data);
            };

            var parseDetails = function (response) {
                var details = _.map(response, function (value) {
                    return {
                        username: value.username,
                        internship: value.internship,
                        email: value.email,
                        phone_number: value.phone_number,
                        evaluated:value.evaluated,

                    };
                });
                return details;
            };
            parseAnswers = function (response) {
                var answers = _.map(response, function (value) {
                    return {
                        questions: value.questions,
                        answer: value.answer
                    };
                });
                return answers;
            }



            this.show = function (show) {
                this.loadDetails();
                this.loadAnswers();
                this.loadComments();
                this.setData({
                    visible: show,
                });
            };

            this.setCandidateID = function (candidateID) {
                this.setData({
                    candidateID: candidateID
                })
            };
            this.loadDetails = function () {
                setTimeout(function () {
                    var details = DETAILS;
                    This.setData({
                        details: details
                    });

                }, 100);
            },
                this.loadAnswers = function () {
                    setTimeout(function () {
                        var answers = ANSWERS;
                        This.setData({
                            answers: answers
                        });

                    }, 100);
                },
                this.loadComments = function () {
                    setTimeout(function () {
                        var comments = COMMENTS;
                        This.setData({
                            comments: comments
                        });

                    }, 100);
                }

        }
    });

});