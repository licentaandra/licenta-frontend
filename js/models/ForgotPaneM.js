define([], function() {

    return Backbone.Model.extend({

        defaults: {
             visible: null,
        },

        initialize: function() {
             var This = this;
            var router = options.router;
            this.setData = function(data) {
                this.set(data);
            };
             this.show = function (show) {
                this.setData({
                    visible: show,
                });
            };

        }

    });

});