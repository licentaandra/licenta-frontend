define([], function () {

    return Backbone.Model.extend({

        defaults: {
            visible: null,
            authenticated: null,
            selectedTab: null,
            candidatesList: null,
            filteredCandidates: null,
            option: null,
        },

        initialize: function (options) {
            var This = this;
            var router = options.router;

            this.setData = function (data) {
                this.set(data);
            };
            this.loadCandidates = function () {

                setTimeout(function () {
                    var candidatesList = CLIENTS3;
                    This.setData({
                        candidatesList: candidatesList
                    });
                }, 100);
            }

            this.parseEvaluated = function (candidates) {
                var evaluated = [];
                for (candidate in candidates) {
                    if (candidates[candidate].evaluated) {
                        evaluated.push(candidates[candidate]);
                    }
                }
                return evaluated;
            };
            this.parseNonEvaluated = function (candidates) {
                var nonevaluated = [];
                for (candidate in candidates) {
                    if (!candidates[candidate].evaluated) {
                        nonevaluated.push(candidates[candidate]);
                    }
                }
                return nonevaluated;
            };
            this.internshipOptions = function (candidates) {
                var options = [];
                for (candidate in candidates) {
                    internshiplist = candidates[candidate].internship;
                    for (internship in internshiplist) {
                        if (options.indexOf(internshiplist[internship]) == -1) {
                            options.push(internshiplist[internship]);
                        }
                    }
                }
                return options;
            };
            this.show = function (show) {
                this.loadCandidates();
                this.setData({
                    visible: show,
                });
            };
            this.loadFilteredCandidates = function (selectedTab) {
                candidatesList = this.get('candidatesList');
                if (selectedTab === null || selectedTab === 'undefined') {
                    This.setData({
                        selectedTab: 'evaluated',
                    });
                    this.loadFilteredCandidates('evaluated');
                }
                else {
                    if (selectedTab == 'notEvaluated') {
                        var nonEvaluated = this.parseNonEvaluated(candidatesList);
                        This.setData({
                            filteredCandidates: nonEvaluated
                        });
                    }
                    else if (selectedTab == 'evaluated') {
                        var evaluated = this.parseEvaluated(candidatesList);
                        This.setData({
                            filteredCandidates: evaluated
                        });
                    }
                    router.candidates_reflectPath_selectedTab({
                        selectedTab: selectedTab
                    });
                }
            };
            this.loadCandidatesByOption = function (option) {
                This.setData({
                            option: option
                        });
                var candidates = this.get('filteredCandidates');
                if (option == 'all') {
                    return candidates;
                }
                
                var newCandidatesList = [];
                for (candidate in candidates) {
                    internships = candidates[candidate].internship;
                    for (internship in internships) {
                        if (internships[internship] == option) {
                            newCandidatesList.push(candidates[candidate]);
                        }
                    }
                }
                return newCandidatesList;
            }


        }
    });

});