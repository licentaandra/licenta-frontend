define([], function () {

    return Backbone.Model.extend({
        
        defaults: {
            visible: null,
            photos: [],
        },

        initialize: function () {
            var This = this;
            var router = options.router;
            this.setData = function (data) {
                this.set(data);
            };
            this.show = function (show) {
                this.setData({
                    visible: show,
                    photos: [],
                });
            };
            this.loadPhotos = function (newPhotos) {

                tmpPhotos = [];
                tmpPhotos = tmpPhotos.concat(this.get('photos'));
                tmpPhotos = tmpPhotos.concat(newPhotos);

                this.setData({
                    photos: tmpPhotos,
                });
            }

            this.sendAlbum = function (name, index, authenticationToken) {
                photos = this.get('photos');
                console.log(authenticationToken);

                var formData = new FormData();
                formData.append('name', name);
                formData.append('cover', index);
                for (i in photos) {
                    formData.append(i, photos[i]);
                }
                console.log(formData);
                var url = API_DOMAIN + 'user_photos';
                $.ajax({
                    url: url,
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    mimeType: "multipart/form-data",
                    data: formData,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                        $("#album_same").css("display", "none");
                        $("#album_name").css("display", "none");
                        $("#no_album").css("display", "none");
                        $('#uploadName').val('');

                    },
                    error: function (data) {
                        $("#album_same").css("display", "-webkit-box");
                    }
                });
            }
        }
    });
});