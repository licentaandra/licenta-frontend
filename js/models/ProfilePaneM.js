define([], function () {

    return Backbone.Model.extend({

        defaults: {
            visible: null,
            details: null,
            comments: null,
            id: null,
            photos: null,
            videos: null,
        },

        initialize: function (options) {

            var This = this;
            var router = options.router;
            this.setData = function (data) {
                this.set(data);
            };
            this.setId = function(id){
                 This.setData({
                            id: id
                        });
            }
            this.loadDetails = function () {
                id = this.get('id');
                var url = API_DOMAIN + 'user_details?id=' + id;
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                        This.setData({
                            details: data
                        });
                    }
                });
            };

            this.booking = function (values) {

                authenticationToken = router.getCookie("authenticated");
                var url = API_DOMAIN + 'bookings';
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(values),
                    processData: false,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                        console.log('All good');

                    },
                    error: function (data) {
                        console.log('error');
                    }
                });
            }

            this.postComment = function (text, stars) {
                id = this.get('id');
                authenticationToken = router.getCookie("authenticated");
                formValues = {
                    text: text,
                    user_to: id,
                    rating: stars
                }
                var url = API_DOMAIN + 'comments';
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(formValues),
                    processData: false,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                        $('#text_comment').val('');
                        $("input[type='radio'].rating-input").prop('checked', false);
                        This.loadComments();

                    },
                    error: function (data) {

                    }
                });
            }
            this.loadComments = function () {
                id = this.get('id');
                var url = API_DOMAIN + 'comments?id=' + id;
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                        This.setData({
                            comments: data
                        });
                    }
                });
            }
            this.loadPhotos = function () {

                id = this.get('id');
                var url = API_DOMAIN + 'user_photos?id=' + id;
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                        console.log('poze', data);
                        This.setData({
                            photos: data
                        });
                    }
                });

            }
            this.loadVideos = function () {
                id = this.get('id');
                var url = API_DOMAIN + 'user_videos?id=' + id;
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                        console.log('data',data)
                        This.setData({
                            videos: data
                        });
                    }
                });
                // setTimeout(function () {
                //     var videos = VIDEOS;
                //     This.setData({
                //         videos: videos
                //     });
                // }, 100);
            }

            this.show = function (show) {

                this.setData({
                    visible: show,
                });
            };
        }

    });

});