define([], function () {

    return Backbone.Model.extend({

        defaults: {
            visible: null,
            events: null,
        },
        initialize: function (options) {
            var This = this;
            var router = options.router;
            this.setData = function (data) {
                this.set(data);
            };
            this.show = function (show) {
                this.setData({
                    visible: show,
                });
                loadEvents();
            };
            loadEvents = function () {
                var url = API_DOMAIN + 'events';
                var authenticationToken = router.getCookie("authenticated");
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {
                        console.log(data);
                        This.setData({
                            events: data

                        });
                    }
                });
            }

        }
    });

});