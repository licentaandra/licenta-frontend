define([], function () {

    return Backbone.Model.extend({

        defaults: {
            visible: null,
            state: null,
        },

        initialize: function (options) {

            var router = options.router;
            var This = this;
            this.setData = function (data) {
                this.set(data);
            };
            this.show = function (show) {
                this.setData({
                    visible: show,
                });
            };
            this.setState = function (state) {
                this.setData({
                    state: state
                })
            };
            this.sendAccountDetails = function (formValues,token) {
                console.log(token);
                var url = API_DOMAIN + 'user_details';
                var authenticationToken = token;
                $.ajax({
                     url: url,
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    mimeType: "multipart/form-data",
                    data: formValues,
                    headers: { 'Authorization': authenticationToken },
                    processData: false,
                    success: function (data) {

                        console.log('All good');
                         router.home_enter();

                    },
                    error: function (data) {
                        console.log('error');
                    }
                });
            }



        }

    });

});