define([], function() {

    return Backbone.Model.extend({

        defaults: {
            selectedItem: null,
            authenticated:null,
            
        },

        initialize: function() {

            this.setData = function(data) {
                this.set(data);
            };

            this.setSelectedItem = function(selectedItem) {

                this.setData({
                    selectedItem: selectedItem
                });

            };

            this.resolveAuthentication = function(authenticated){
                this.setData({
                    authenticated: authenticated
                });
            }


        }

    });

});