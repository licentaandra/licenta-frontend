define([], function() {

    return Backbone.Model.extend({

        initialize: function() {

            var router = this.attributes.router;
            var section = null;
            var selectedTab = null;
            var id = null;


            this.updateUrl = function() {
                var segments = [
                    section
                ];
                    if  (section == SECTIONS.candidates) 
                    {
                        segments.push('selectedtab='+ selectedTab);
                    }
                    
                     if  (section == SECTIONS.profile) 
                    {
                        segments.push('photograph='+ id);
                    }
                router.navigate(segments.join('/'));
            };

            this.updateSection = function(newSection) {
                section = newSection;
            };
             this.setSelectedTab = function(newSelectedTab ) {
                selectedTab= newSelectedTab;
            };
             this.setID = function(newID ) {
                id= newID;
            };
             

        }

    });

});