define([], function() {

    return Backbone.View.extend({

        el: $("#FORGOT"),

        events: {
               'click #btn_forgot': 'sendEmail',
        },

        initialize: function() {

            var router = this.options.router;
            var This = this;

            var reflectChange_visible = function(model, visible) {
                if (visible) {
                    this.$el.show();
                } else {
                    this.$el.hide();
                }
            };

            this.listenTo(this.model, 'change:visible', reflectChange_visible);
        },

       sendEmail:function(){
                var email =  document.getElementById("forgot_email").value;
                alert(email);
        }

    });

});