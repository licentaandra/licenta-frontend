define(['magnificPopup'], function (magnificPopup, lightbox) {

    return Backbone.View.extend({

        el: $("#PROFILE"),
        events: {
            'click #about': 'showAbout',
            'click #show_photos': 'showPhotos',
            'click #show_videos': 'showVideos',
            'click #comment': 'postComment',
            'click .photo-item': 'showAlbum',
            'click #showBook': 'showBook',
            'click #req_book': 'requestBooking',
            'click #btn_req_book':'showBook'
        },
        initialize: function () {

            var router = this.options.router;
            var This = this;

            var reflectChange_visible = function (model, visible) {
                if (visible) {
                    this.$el.show();
                    this.model.loadDetails();
                    this.model.loadComments();
                } else {
                    this.$el.hide();
                }
            };
            var reflectChange_details = function (model, details) {
                console.log(details);
                var coverPhoto = this.$el.find('#coverPhoto');
                coverPhoto.empty();
                // coverPhoto.append(
                //     '	<img src="' + details.img + '" alt="Profile Cover">'
                // );
                id=this.model.get('id');
                coverPhoto.append(
                    '	<img src="' + 'http://localhost:8080/public/'+id+'/cover.jpeg' + '" alt="Profile Cover" style="max-width: 100%;max-height: 100%;">'
                );
                var country_id = this.$el.find('#country_profile');
                country_id.empty();
                country_id.append(details.region);
                var name = this.$el.find('#name_profile');
                name.empty();
                name.append(details.f_name);
                var profilePicture = this.$el.find('#profilePicture');
                // profilePicture.append(
                //     '	<img src="' + details.profile_pic + '" alt="Profile Picture">'
                // );
                var media = this.$el.find('#media_btn');
                media.empty();
                media.append(
                    '<a href="' + details.links.facebook + '" target="_blank" class="btn btn-control" style="background-color:#2f5b9d"><div class="fa fa-facebook"></div></a>'
                );
                media.append('<a href="' + details.links.instagram + '" target="_blank" class="btn btn-control" style="background-color:#ff0080;"><div class="fa fa-instagram" style="color:#ffffff;"></div></a>');

                var photol = this.$el.find('#photography_likes');
                photol.empty();
                p = details.types;
                for (photo in p) {
                    item = PTypes[p[photo]];
                    photol.append('<div class="icons"><i class="fa ' + item.icon + '" aria-hidden="true"></i>\
                     <p>'+ item.name + '</p></div>');

                }
                var about = this.$el.find('#photo_descr');
                about.empty();
                about.append(details.description);
                var about = this.$el.find('#photo_loc');
                about.empty();
                about.append(details.region);
                var about = this.$el.find('#photo_gen');
                about.empty();
                about.append(details.region);
                var about = this.$el.find('#photo_em');
                about.empty();
                about.append(details.email);
                var about = this.$el.find('#photo_ph');
                about.empty();
                about.append(details.phone);
                var about = this.$el.find('#photo_price');
                about.empty();
                about.append(details.price + '$');
            };
            var reflectChange_comments = function (model, comments) {
                console.log('com',comments);
                this.$el.find('#average_comm').append(comments.avg_rating + '/7');
                var list = this.$el.find('#comments_p');
                items = comments.comments;
                for (c in items) {
                    det = items[c];
                    var new_elem = '<span class="rating">';
                    for (j = 7; j >= 0; j--) {
                        if (j <= items[c].rating) {
                            new_elem += '<label class="rating-star-yellow" ></label>'
                        }
                        else {
                            new_elem += '<label class="rating-star-white" ></label>'
                        }
                    }
                    new_elem += '</span>';
                    list.append('<ul class="comments-list" id="comments_p"><li>\
											<div class="post__author author vcard inline-items">\
												<img src="img/author-page.jpg" alt="author">\
												<div class="author-date">\
													<div class="h6 post__author-name fn" >'+ det.f_name + '</div>\
                                                    '+ new_elem + '\
													<div class="post__date">'+ det.date + '</div>\
													</div></div><p>'+ det.text + '</p></li></ul>');

                }


            };

            var reflectChange_photos = function (model, photos) {
                var album = this.$el.find('#photo_album');
                for (p in photos) {
                    id = this.model.get('id');
                    var url = API_DOMAIN + 'public/' + id + '/' + p + '/' + photos[p].cover;

                    album.append('<div class="photo-item half-width" id=' + p + '>\
								<img src='+ url + ' alt="photo">\
												<div class="overlay overlay-dark"></div>\
												<a href="#" class="more"></a>\
												<a href="#" class="post-add-icon inline-items">\
												</a>\
												<div class="content">\
													<a href="#" class="h6 title">'+ p + '</a>\
													<time class="published" datetime="2017-03-24T18:18">1 week ago</time>\
												</div></div>');


                }

            };
            var reflectChange_videos = function (model, videos) {
                console.log(videos);
                var v_album = this.$el.find('#video_album');
                v_album.empty();
                for (i in videos) {
                    v_album.append('<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">\
                    			<div class="ui-block video-item">\
                    				<div class="video-player">\
                    					<img src="'+ videos[i].thumbnail + '" alt="photo">\
                    					<a href="'+ videos[i].link + '" class="play-video play-video--small">\
                    						<svg class="olymp-play-icon">\
                    							<use xlink:href="icons/icons.svg#olymp-play-icon"></use>\
                    						</svg></a>\
                    					<div class="overlay overlay-dark"></div></div>\
                    				<div class="ui-block-content video-content">\
                    					<div class="h6 title">'+ videos[i].title + '</div>\
                    				</div></div></div>');
                }
                this.videoOpen();
            };

            this.listenTo(this.model, 'change:visible', reflectChange_visible);
            this.listenTo(this.model, 'change:details', reflectChange_details);
            this.listenTo(this.model, 'change:comments', reflectChange_comments);
            this.listenTo(this.model, 'change:photos', reflectChange_photos);
            this.listenTo(this.model, 'change:videos', reflectChange_videos);

        },
        requestBooking: function () {
            user_to = this.model.get('id');
            time = $('#day_time').find(":selected").val();
            time_f = parseInt(time) + 3;
            str = $('#req_id').val();
            var d = str.substring(0, 2),
                m= str.substring(3,5);
                y = str.substring(6, 10);
            new_date=y+'-'+m+'-'+d;
            var type = $('#p_type_book option:selected').val();         
            var values = {
                user_to: user_to,
                start_date: new_date + ' ' + time + ':00:00',
                end_date: new_date + ' ' + time_f + ':00:00',
                message: $('#descr_u').val(),
                type:type
            }
             this.model.booking(values);
        },

        showBook: function () {
            $("#about_profile").css("display", "none");
            $("#photos_profile").css("display", "none");
            $("#video_profile").css("display", "none");
            $("#album_profile").css("display", "none");
            $("#book_profile").css("display", "-webkit-box");
            var TElement = document.getElementById('p_type_book');
                TElement.length = 0;
                TElement.selectedIndex = 0;
                for (var i = 0; i < PTypes.length; i++) {
                    TElement.options[TElement.length] = new Option(PTypes[i].name, PTypes[i].id);
                }

        },

        showAbout: function () {
            $("#about_profile").css("display", "-webkit-box");
            $("#photos_profile").css("display", "none");
            $("#video_profile").css("display", "none");
            $("#album_profile").css("display", "none");
            $("#book_profile").css("display", "none");
        },
        showPhotos: function () {
            $("#about_profile").css("display", "none");
            $("#photos_profile").css("display", "-webkit-box");
            $("#video_profile").css("display", "none");
            $("#album_profile").css("display", "none");
            $("#book_profile").css("display", "none");

            this.model.loadPhotos();
        },
        showVideos: function () {
            $("#about_profile").css("display", "none");
            $("#photos_profile").css("display", "none");
            $("#video_profile").css("display", "-webkit-box");
            $("#album_profile").css("display", "none");
            $("#book_profile").css("display", "none");

            this.model.loadVideos();
        },
        postComment: function () {
            text = $('#text_comment').val();
            var stars = $("input[type='radio'].rating-input:checked").val();
            this.model.postComment(text, stars);
        },
        showAlbum: function (event) {
            var id = event.currentTarget.id;
            $("#about_profile").css("display", "none");
            $("#photos_profile").css("display", "none");
            $("#video_profile").css("display", "none");
            $("#album_profile").css("display", "-webkit-box");
            $("#book_profile").css("display", "none");

            photos = this.model.get('photos');
            id_pers = this.model.get('id');
            album = photos[id].images;
            var url = API_DOMAIN + 'public/' + id_pers + '/' + id + '/';

            var view = this.$el.find('#render_album');
            view.empty();
            for (i in album) {
                console.log(album[i])
                var new_url = url + album[i];
                view.append('<a class="example-image-link" href=' + new_url + ' data-lightbox="example-set"><img class="example-image photo-item half-width" src=' + new_url + ' alt=""/></a>')
            }


        },
        videoOpen: function () {
            $('.play-video').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });
        }


    });
});