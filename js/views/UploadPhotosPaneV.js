define([], function () {

    return Backbone.View.extend({

        el: $("#UPLOADPHOTOS"),
        photoTemplate: _.template($('#tpl_photo_upload').html()),

        events: {
            'change #inputPhoto': "changeInputEventHandler",
            'click #uploadDiscard': "uploadDiscard",
            'click #uploadSave': "uploadSave"
        },

        initialize: function () {

            var router = this.options.router;
            var This = this;

            var reflectChange_visible = function (model, visible) {
                if (visible) {
                    this.$el.show();
                    var panelElement = this.$el.find('#UploadPhotos');
                    panelElement.empty();
                } else {
                    this.$el.hide();
                }
            };

            //   var reflectChange_photos = function (model, photo) {
            //               };

            this.changeInputEventHandler = function () {
                newPhotos = [];
                inputPhotos = $('#inputPhoto').get(0).files;
                var panelElement = this.$el.find('#UploadPhotos');
                for (i = 0; i < inputPhotos.length; i++) {
                    reader = new FileReader();
                    reader.readAsDataURL(inputPhotos[i]);
                    newPhotos.push(inputPhotos[i]);
                    reader.onload = function (e) {

                        panelElement.append(This.photoTemplate({
                            photo: e.target.result,
                        }))
                    };
                }

                this.model.loadPhotos(newPhotos);
            }

            this.uploadSave = function () {
                name = $('#uploadName').val();
                index = 0;
                 $(".coverRadios").each(function (i) {
                    if (this.checked) {
                        index = i;
                    }
                });
                authenticationToken = router.getCookie('authenticated');
                photos = this.model.get('photos');
               
               if(photos.length == 0){
                     $("#no_album").css("display", "-webkit-box"); 
                     $("#album_name").css("display", "none");

                } else if(name == ''){
                      $("#album_name").css("display", "-webkit-box"); 
                       $("#no_album").css("display", "none");
                } 
               else if(name != '' || photos.length != 0){
                     this.model.sendAlbum(name,index,authenticationToken);
                     this.uploadDiscard();
                      $("#album_name").css("display", "none"); 
                }                
            }
            this.uploadDiscard = function () {
                var panelElement = this.$el.find('#UploadPhotos');
                panelElement.empty();
                this.model.setData({photos:[]});
            }

            this.listenTo(this.model, 'change:visible', reflectChange_visible);
            //  this.listenTo(this.model, 'change:photos', reflectChange_photos);
        },

    });

});