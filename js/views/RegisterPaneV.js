define([], function() {

    return Backbone.View.extend({

        el: $("#REGISTER"),

        events: {
               'click #btn_register_photog':'register_photographer',
              'click #btn_register_user':'register_user',
              'click #btn_register_company':'register_company',
        },

        initialize: function() {

            var router = this.options.router;
            var This = this;

            var reflectChange_visible = function(model, visible) {
                if (visible) {
                    this.$el.show();
                } else {
                    this.$el.hide();
                }
            };

            this.register_photographer = function(){
                 router.parentPane_toggle(SECTIONS.r_photographer);
            };
             this.register_user = function(){
                  router.parentPane_toggle(SECTIONS.r_user);
             };
              this.register_company = function(){
                   router.parentPane_toggle(SECTIONS.r_company);
              };
            this.listenTo(this.model, 'change:visible', reflectChange_visible);
        },

    });

});