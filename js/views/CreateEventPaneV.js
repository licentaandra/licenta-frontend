define([], function () {

    return Backbone.View.extend({

        el: $("#CREATEEVENT"),

        events: {

            "click #create_ev_btn": "sendEvent",
             'change #event_photo': function () { this.profile_photo('#event_photo', '#ievent_photo'); },
        },

        initialize: function () {

            var router = this.options.router;
            var This = this;

            var reflectChange_visible = function (model, visible) {
                if (visible) {
                    this.$el.show();
                } else {
                    this.$el.hide();
                }
            };
            
            this.profile_photo = function (input, destination) {
                inputPhotos = $(input).get(0).files;

                reader = new FileReader();
                reader.readAsDataURL(inputPhotos[0]);
                reader.onload = function (e) {
                    $(destination).attr("src", e.target.result);

                };
            };

            this.listenTo(this.model, 'change:visible', reflectChange_visible);
        },
        sendEvent: function () {
            name = $('#ev_name').val();
            type = $('#create_ev_type').find(":selected").val();
            loc = $('#ev_cr_location').val();
            start_time = $('#cr_ev_start').val();
            end_time = $('#cr_ev_end').val();
            description = $('#create_ev_desc').val();
            volunteers = $('#ev_volunteers').val();
            str = $('#cr_ev_date').val();
            var d = str.substring(0, 2),
                m = str.substring(3, 5);
            y = str.substring(6, 10);
            new_date = y + '-' + m + '-' + d;



            var photo = $('#event_photo').get(0).files[0];

            var details = new FormData();
            details.append('name', name);
            details.append('location', loc);
            details.append('description', description);
            details.append('ev_type', type);
            details.append('start_time', start_time);
            details.append('end_time', end_time);
            details.append('photo', photo);
            details.append('type', 'event');
            details.append('volunteers', volunteers);
            details.append('date', new_date + " 00:00:00");


            this.model.sendEvent(details);

        }

    });

});