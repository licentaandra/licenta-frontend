define(['material'], function (material) {

    return Backbone.View.extend({

        el: $("#ACCOUNTDETAILS"),

        events: {
            "click #btn_register_user": "details_u",
            "click #btn_register_company": "details_c",
            "click #btn_register_photog": "details_p",
            "click #u_btn": "user_submit",
            "click #c_btn": "company_submit",
            "click #p_btn": "photographer_submit",
            'change #PP_U': function () { this.profile_photo('#PP_U', '#iPP_U'); },
            'change #CP_U': function () { this.profile_photo('#CP_U', '#iCP_U'); },
            'change #PP_C': function () { this.profile_photo('#PP_C', '#iPP_C'); },
            'change #CP_C': function () { this.profile_photo('#CP_C', '#iCP_C'); },
            'change #PP_P': function () { this.profile_photo('#PP_P', '#iPP_P'); },
            'change #CP_P': function () { this.profile_photo('#CP_P', '#iCP_P'); },

        },

        initialize: function () {

            var router = this.options.router;
            var This = this;

            var reflectChange_visible = function (model, visible) {
                if (visible) {
                    this.$el.show();
                    var role = router.getRole();
                    This.userDetails(role);
                } else {
                    this.$el.hide();
                }
            };
            this.userDetails = function (role) {
                if (role == 'null' || role == undefined) {
                    $('#REGISTER').css("display", "-webkit-box");
                    $('#REGISTER_COMPANY').css("display", "none");
                    $('#REGISTER_PHOTOGRAPHER').css("display", "none");
                    $('#REGISTER_USER').css("display", "none");
                }
                else if (role == 1) {
                    // regular
                    $('#REGISTER_COMPANY').css("display", "none");
                    $('#REGISTER_PHOTOGRAPHER').css("display", "none");
                    $('#REGISTER').css("display", "none");
                    $('#REGISTER_USER').css("display", "block");

                }
                else if (role == 2) {
                    //photographer
                    $('#REGISTER_USER').css("display", "none");
                    $('#REGISTER_COMPANY').css("display", "none");
                    $('#REGISTER').css("display", "none");
                    $('#REGISTER_PHOTOGRAPHER').css("display", "block");
                    populateCountries("location_p");
                    populateTypes();

                }
                else if (role == 3) {
                    // company
                    $('#REGISTER_USER').css("display", "none");
                    $('#REGISTER_PHOTOGRAPHER').css("display", "none");
                    $('#REGISTER').css("display", "none");
                    $('#REGISTER_COMPANY').css("display", "block");
                    populateCountries("location_c");

                }
            }

            this.profile_photo = function (input, destination) {
                inputPhotos = $(input).get(0).files;

                reader = new FileReader();
                reader.readAsDataURL(inputPhotos[0]);
                reader.onload = function (e) {
                    $(destination).attr("src", e.target.result);

                };
            };

            this.details_p = function () {
                //photographer
                router.setCookie("role", '2', 30);
                router.accountDetails_enter('new');
                This.userDetails('2');
            };
            this.details_c = function () {
                //company
                router.setCookie("role", '3', 30);
                router.accountDetails_enter('new');
                This.userDetails('3');
            };
            this.details_u = function () {
                //regular
                router.setCookie("role", '1', 30);
                router.accountDetails_enter('new');
                This.userDetails('1');
            };
            this.user_submit = function () {
                var token = router.getAuthenticationToken();
                var phone = $('#phone_u').val();
                var role = router.getRole();
                var name = $('#name_u').val();
                var description = $('#descr_us').val();
                var gender = $('#u_gender').find(":selected").val();
                var profile = $('#PP_U').get(0).files[0];
                var cover = $('#CP_U').get(0).files[0];

                console.log(description);
                var details = new FormData();
                details.append('f_name', name);
                details.append('acc_type_id', role);
                details.append('description', description);
                details.append('gender', gender);
                details.append('phone', phone);
                details.append('cover', cover);
                details.append('profile', profile);
                details.append('type', 'details');
                this.model.sendAccountDetails(details, token);


            };
            this.company_submit = function () {
                var profile = $('#PP_C').get(0).files[0];
                var cover = $('#CP_C').get(0).files[0];
                var token = router.getAuthenticationToken();
                var role = router.getRole();
                var phone = $('#phone_c').val();
                var name = $('#name_c').val();
                var description = $('#descr_c').val();
                var clink = $('#link_c').val();
                var location = $('#location_c option:selected').text();
                var details = new FormData();
                details.append('f_name', name);
                details.append('acc_type_id', role);
                details.append('description', description);
                details.append('links', JSON.stringify({'company': clink}));
                details.append('region', location);
                details.append('phone', phone);
                details.append('cover', cover);
                details.append('profile', profile);
                details.append('type', 'details');

                console.log(details);
                this.model.sendAccountDetails(details, token);
            };
            this.photographer_submit = function () {
                var profile = $('#PP_P').get(0).files[0];
                var cover = $('#CP_P').get(0).files[0];
                var token = router.getCookie("authenticated");
                var role = router.getRole();
                var phone = $('#phone_p').val();
                var name = $('#name_p').val();
                var description = $('#desc_p').val();
                var facebook = $('#fl_p').val();
                var instagram = $('#il_p').val();
                var price = $('#price_p').val();
                var location = $('#location_p option:selected').text();
                var gender = $('#p_gender').find(":selected").val();
                var types = [];
                $('#type_p input:checked').each(function () {
                    val = $(this).val();
                    types.push(val);
                });
                console.log(types)

                var details = new FormData();
                details.append('f_name', name);
                details.append('acc_type_id', role);
                details.append('gender', gender);
                details.append('description', description);
                details.append('gender', gender);
                details.append('links', JSON.stringify({
                    facebook: facebook,
                    instagram: instagram,
                }));
                details.append('price', price);
                details.append('types', JSON.stringify(types));
                details.append('region', location);
                details.append('phone', phone);
                details.append('cover', cover);
                details.append('profile', profile);
                details.append('type', 'details');

                console.log(details);
                this.model.sendAccountDetails(details, token);

            };

            populateTypes = function () {
                var types = PTypes;
                panelElement = this.$('#REGISTER_PHOTOGRAPHER').find('#type_p');


                for (type in types) {
                    panelElement.append(' <div class="checkbox"><label><input type="checkbox" name="types" value=' + types[type].id + '>' + types[type].name + '</label></div>')
                }
                $.material.init();

                $('.checkbox > label').on('click', function () {
                    $(this).closest('.checkbox').addClass('clicked');
                })

            };

            populateCountries = function (countryElementId) {
                // given the id of the <select> tag as function argument, it inserts <option> tags
                var countryElement = document.getElementById(countryElementId);
                countryElement.length = 0;
                countryElement.options[0] = new Option('Select Location', '-1');
                countryElement.selectedIndex = 0;
                for (var i = 0; i < country_arr.length; i++) {
                    countryElement.options[countryElement.length] = new Option(country_arr[i], country_arr[i]);
                }

            }


            this.listenTo(this.model, 'change:visible', reflectChange_visible);
        },

    });

});