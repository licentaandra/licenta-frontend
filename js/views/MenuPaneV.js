define([], function () {

    return Backbone.View.extend({

        el: $('#MENU_SECTION'),

        events: {
            'click li': 'onSelectedItemClick',
            'click #logout': 'onLogout',
            'click #video_m': 'videoOverlay',
            'click #save_video': 'saveVideo'
        },

        initialize: function () {

            var router = this.options.router;

            this.reflect_selectedItem = function (model, value) {
                switch (value) {
                    case SECTIONS.login:
                        // this.$el.find('.active').removeClass('active');
                        // this.$el.find('.' + SECTIONS.login).addClass('active');
                        $("#MENU_SECTION").css("display", "none");
                        break;

                }
            };
            this.reflect_authenticated = function (model, authenticated) {
                if (authenticated) {
                    $("#MENU_SECTION").css("display", "inline");
                }
                else {
                    $("#MENU_SECTION").css("display", "none");
                }
            }
            this.videoOverlay = function () {
                $('body').addClass('overlay-enable');
                $("#show_open").addClass("open");
            }
            this.saveVideo = function () {
                link = $('#url_video').val();
                authenticationToken = router.getCookie("authenticated");
                var url = API_DOMAIN + 'user_videos';
                values = { link: link };
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(values),
                    processData: false,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                        console.log('All good');
                        $('#url_video').val("");
                        $('body').removeClass('overlay-enable');
                        $("#show_open").removeClass("open");
                    },
                    error: function (data) {
                        console.log('error');
                    }
                });
            }
            this.Logout = function () {

                document.cookie = "authenticated=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                document.cookie = "role=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                router.setAuthenticationToken();
                router.parentPane_toggle(SECTIONS.login);
            }
            this.handleChange_selectedItem = function (section) {
                this.model.setSelectedItem(section);
                router.parentPane_toggle(section);
            };

            this.listenTo(this.model, 'change:selectedItem', this.reflect_selectedItem);
            this.listenTo(this.model, 'change:authenticated', this.reflect_authenticated);

        },

        onSelectedItemClick: function (event) {
            var section = event.currentTarget.dataset.section;
            this.handleChange_selectedItem(section);
        },
        onLogout: function () {
            this.Logout();
        },

    });

});