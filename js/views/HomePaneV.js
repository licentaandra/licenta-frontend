define([], function () {

    return Backbone.View.extend({

        el: $("#HOME"),
        homeTemplate: _.template($('#tpl_profile').html()),

        events: {
            'click #filterButton': "filterResults",
            'click .route_profile':"routeProfile"
        },

        initialize: function (options) {

            var router = this.options.router;
            var This = this;

            this.routeProfile = function(event){
                var id = event.currentTarget.id;
                newid =  id.substr(2);
                router.profile_enter(newid);

            }

            this.filterResults = function () {
                index = 0;
                $(".filterRadios").each(function (i) {
                    if (this.checked) {
                        index = i;
                    }
                });
                if(index == 0){
                    sort = 'rating';
                }
                else {
                    sort = 'price';
                }
                var region = $('#location_select option:selected').text();
                var type = $('#photo_select option:selected').val();
                console.log(type);
                this.model.loadSearch(region,type,sort);
            }
            var reflectChange_visible = function (model, visible) {
                if (visible) {
                    this.$el.show();
                    this.model.loadProfile();
                     $("#MENU_SECTION").css("display", "inline");
                    var typeElement = document.getElementById('photo_select');
                    typeElement.length = 0;
                    typeElement.options[0] = new Option('Choose a photography style', '-1');
                    typeElement.selectedIndex = 0;
                    for (var i = 0; i < PTypes.length; i++) {
                        typeElement.options[typeElement.length] = new Option(PTypes[i].name, PTypes[i].id);
                    }
                    populateCountries("location_select");
                } else {
                    this.$el.hide();
                }
            };
            var reflectChange_profile = function (model, profile) {
                var panelElement = this.$el.find('#HomeProfile');
                panelElement.empty();
                truncateDecimals = function (number) {
                    return Math[number < 0 ? 'ceil' : 'floor'](number);
                };
                for (p in profile) {
                    var person = profile[p]
                    var rating = truncateDecimals(person.rating * 100) / 100;
                    panelElement.append(This.homeTemplate({
                        photo: person.photo,
                        id:person.id,
                        name: person.f_name,
                        location: person.location,
                        rating: rating,
                        comments: person.comments,
                        price: person.price + ' $',
                        image:'http://localhost:8080/public/'+person.id+'/profile.jpeg'
                    }))

                }

            };

            populateCountries = function (countryElementId) {
                var countryElement = document.getElementById(countryElementId);
                countryElement.length = 0;
                countryElement.options[0] = new Option('Choose a destination', '-1');
                countryElement.selectedIndex = 0;
                for (var i = 0; i < country_arr.length; i++) {
                    countryElement.options[countryElement.length] = new Option(country_arr[i], country_arr[i]);
                }

            }

            this.listenTo(this.model, 'change:visible', reflectChange_visible);
            this.listenTo(this.model, 'change:profile', reflectChange_profile);
        },


    });

});