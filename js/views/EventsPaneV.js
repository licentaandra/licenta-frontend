define([], function () {

    return Backbone.View.extend({

        el: $("#EVENTS"),
        eventTemplate: _.template($('#tpl_list_events').html()),

        events: {
            "click .conf-ev ": "respondInvite",
        },

        initialize: function () {

            var router = this.options.router;
            var This = this;

            this.respondInvite = function (event) {
                var req_id = event.currentTarget.id;
                id = req_id.substr(2);
                authenticationToken = router.getCookie("authenticated");
                var url = API_DOMAIN + 'events';
                val = {event_id:id};
                $.ajax({
                    url: url,
                    type: 'PUT',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(val),
                    processData: false,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                        console.log('registered');

                    },
                    error: function (data) {
                        console.log('error');
                    }
                });

            }
            var reflectChange_visible = function (model, visible) {
                if (visible) {
                    this.$el.show();
                } else {
                    this.$el.hide();
                }
            };

            var reflectChange_events = function (model, events) {

                var req = this.$el.find('#events_list');
                req.empty();

                for (e in events) {
                    img_list = ['img/concert.png', 'img/festival.png', 'img/conference.png', 'img/sport.png']
                    req.append(This.eventTemplate({
                        id: events[e].id,
                        name: events[e].name,
                        description: events[e].description,
                        date: events[e].date,
                        start_time: events[e].start_time,
                        end_time: events[e].end_time,
                        location: events[e].location,
                        image: img_list[events[e].type]
                    }))
                }
            };

            this.listenTo(this.model, 'change:visible', reflectChange_visible);
            this.listenTo(this.model, 'change:events', reflectChange_events);
        },

    });

});