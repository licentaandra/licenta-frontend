define([], function () {

    return Backbone.View.extend({

        el: $("#LOGIN"),

        events: {
            "click #loginButton": "login",
            "click #fpass": "forgot",
            "click #b_log": "show_login",
            "click #b_reg": "show_register",
            "click #back_reg": "show_register",
            "click #back_log": "show_login",
            "click #registerButton": "register",
            "click #btn_register_user": "details_u",
            "click #btn_register_company": "details_c",
            "click #btn_register_photog": "details_p",

        },

        initialize: function () {

            var router = this.options.router;
            var This = this;

            this.login = function () {
                $('.alert-error').hide(); // Hide any errors on a new submit
                var formValues = {
                    username: $('#inputUsername').val(),
                    password: $('#inputPassword').val()
                };
                this.model.loadAuthenticated(formValues);

            };
            this.register = function(){
                $('.alert-error').hide(); // Hide any errors on a new submit
                if( $('#inputPasswordR').val() ==  $('#inputPasswordRS').val()){
                      var formValues = {
                    email: $('#inputEmailR').val(),
                    username: $('#inputUsernameR').val(),
                    password: $('#inputPasswordR').val()
                };
                console.log(formValues);
                this.model.loadRegister(formValues);
                //asta trebuie facut pe raspunsul de tip de account null
                // $("#login_box").css("display", "none");
                // $("#register_box").css("display", "none");
                // $("#REGISTER").css("display", "-webkit-box");

                }
                else{
                     $('.alert-error').text("The passwords do not match").show();
                }
            }

            this.forgot = function () {
                router.parentPane_toggle(SECTIONS.forgot);
            };

            this.show_login = function () {
                $("#login_box").css("display", "-webkit-box");
                $("#register_box").css("display", "none");
                $("#b_all").css("display", "none");
            };

            this.show_register = function () {
                $("#login_box").css("display", "none");
                $("#register_box").css("display", "-webkit-box");
                $("#b_all").css("display", "none");
            };

            var reflectChange_visible = function (model, visible) {
                if (visible) {
                    this.$el.show();
                     $("#MENU_SECTION").css("display", "none");
                } else {
                    this.$el.hide();
                }
            };

            this.listenTo(this.model, 'change:visible', reflectChange_visible);
        },

    });

});