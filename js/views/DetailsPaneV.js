define([], function () {

    return Backbone.View.extend({
        el: $('#DETAILS'),
        detailslistTemplate: _.template($('#tpl_details').html()),
        questionslistTemplate: _.template($('#tpl_details_answers').html()),
        commentslistTemplate: _.template($('#tpl_comments').html()),

        events: {
            'click #submit': 'addComment',
        },

        initialize: function () {

            var router = this.options.router;
            var This = this;

            var reflectChange_visible = function (model, visible) {

                if (visible) {
                    this.$el.show();
                } else {
                    this.$el.hide();
                }
            };
            var reflectChange_details = function (model, details) {
                var panelElement = this.$el.find('#DETAILS_BOX');
                var buttonEvaluated = this.$el.find('#btn_evaluated');
                panelElement.empty();
                panelElement.append(This.detailslistTemplate({
                    name: details.username,
                    internship: details.internship,
                    email: details.email,
                    phone_number: details.phone_number,
                }));
               
            }

            var reflectChange_answers = function (model, answers) {
                var panelElement = this.$el.find('#QUESTION_BOX');
                panelElement.animate({},
                    function () {
                        panelElement.empty();
                        for (var i = 0; i < answers.length; i++) {
                            var answerItem = answers[i];
                            panelElement.append(This.questionslistTemplate({
                                question: answerItem.question,
                                answer: answerItem.answer
                            }));
                        }
                    })

            }

            var reflectChange_comments = function (model, comments) {
                var panelElement = this.$el.find('#COMMENTS');
                panelElement.animate({},
                    function () {
                        panelElement.empty();
                        for (var i = 0; i < comments.length; i++) {
                            var commentItem = comments[i];
                            var new_elem = '<span class="rating">';
                            for (j = 7; j >= 0; j--) {
                                if (j <= commentItem.rating) {
                                    new_elem += '<label class="rating-star-yellow" ></label>'
                                }
                                else {
                                    new_elem += '<label class="rating-star-white" ></label>'
                                }
                            }
                            new_elem += '</span>';
                            panelElement.append(This.commentslistTemplate({
                                id: commentItem.id,
                                name: commentItem.name,
                                description: commentItem.description,
                                stars: new_elem,

                            }));
                        }
                    })
            }


            this.listenTo(this.model, 'change:visible', reflectChange_visible);
            this.listenTo(this.model, 'change:details', reflectChange_details);
            this.listenTo(this.model, 'change:answers', reflectChange_answers);
            this.listenTo(this.model, 'change:comments', reflectChange_comments);

        },
        addComment: function () {
            var comment_text = $('#review').text();
            var stars = $("input[type='radio'].rating-input:checked").val();
            alert(stars);
        },

    });

});