define([], function () {

    return Backbone.View.extend({
        el: $('#CANDIDATES'),
        candidatelistTemplate: _.template($('#tpl_candidates_list').html()),

        events: {
            'click li': 'showDetails',
            'click #evaluated': 'showEvaluated',
            'click #not_evaluated': 'showNotEvaluated',
            'click #options': 'showOptions',
            'click #CANDIDATES_OPTION input': 'sortByOption',
        },

        initialize: function () {

            this.router = this.options.router;
            var This = this;

            var reflectChange_visible = function (model, visible) {

                if (visible) {
                    this.$el.show();
                    selectedTab = this.model.get('selectedTab');
                    this.model.loadCandidates();
                } else {
                    this.$el.hide();
                }
            };
            this.reflectChange_filteredCandidates = function (model, filteredCandidates) {
                var option = this.model.get('option');
                var panelElement = this.$el.find('#CANDIDATES_LIST');
                panelElement.empty();
                if (filteredCandidates.length==0) { 
                    panelElement.append('<h2 style="margin-left:35%;margin-top:100px;">There is no candidate in this section .</h2>');
                }
                else {
                    for (var i = 0; i < filteredCandidates.length; i++) {
                        var candidate = filteredCandidates[i];
                        for (j in candidate.internship) {
                        if( option != 'all' && option != null ){
                            if(  candidate.internship[j] == option){
                               panelElement.append(This.candidatelistTemplate({
                                name: candidate.name,
                                internship: candidate.internship[j],
                                id: candidate.id,
                                email: candidate.email,
                            }));
                        }}
                          else{
                                panelElement.append(This.candidatelistTemplate({
                                name: candidate.name,
                                internship: candidate.internship[j],
                                id: candidate.id,
                                email: candidate.email,
                            }));
                        } 
                        }
                    }
                }
                var paginatedList = new List('CANDIDATES', {
                    valueelementlis: ['elementli'],
                    page: 6,
                    plugins: [ListPagination({})]
                });
                
            }
            var reflectChange_selectedTab = function (model, selectedTab) {
                if (selectedTab == 'evaluated') {
                    this.$el.find('#not_evaluated').removeClass('active');
                    this.$el.find('#evaluated').addClass('active');
                    this.model.set()
                }
                else {
                    this.$el.find('#evaluated').removeClass('active');
                    this.$el.find('#not_evaluated').addClass('active');
                }
                this.model.loadFilteredCandidates(selectedTab);
            }
            reflectChange_candidatesList = function (model, candidatesList) {
                this.model.loadFilteredCandidates(this.model.get('selectedTab'));
            }

            this.listenTo(this.model, 'change:visible', reflectChange_visible);
            this.listenTo(this.model, 'change:selectedTab', reflectChange_selectedTab);
            this.listenTo(this.model, 'change:filteredCandidates', this.reflectChange_filteredCandidates);
            this.listenTo(this.model, 'change:candidatesList', reflectChange_candidatesList);


        },
        showDetails: function (event) {
            var id = event.currentTarget.id;
            this.router.details_enter(id);

        },
        showNotEvaluated: function () {
            this.model.set({
                selectedTab: "notEvaluated",
                option:"all"
            });
            $("#all").prop('checked', true);
        },
        showEvaluated: function () {
            this.model.set({
                selectedTab: "evaluated",
                option:"all"
            });
            $("#all").prop('checked', true);
        },
        showOptions: function () {
            candidates = this.model.get('candidatesList');
            var panelElement = this.$el.find('#CANDIDATES_OPTION');
            panelElement.empty();
            var options = this.model.internshipOptions(candidates);
            for (option in options) {
                panelElement.append('<div> <input type="radio" name="role" value="supervisor" id=' + options[option] + '><label for=' + options[option] + '>' + options[option] + '</label></div>');
            }

            panelElement.append('<div><input type="radio" name="role" value="supervisor" id="all" checked="true"><label for="all">' + 'all candidates' + '</label></div>');
        },
        sortByOption: function (event) {
            option = event.currentTarget.id;
            candidatesByOption = this.model.loadCandidatesByOption(option);
            this.reflectChange_filteredCandidates(this.model, candidatesByOption);

        }


    });

});