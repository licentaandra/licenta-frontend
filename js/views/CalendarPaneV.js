define(['fullcalendar', 'Chart'], function (fullcalendar, Chart) {

    return Backbone.View.extend({

        el: $("#CALENDARP"),
        reqTemplate: _.template($('#tpl_requests').html()),
        historyTemplate: _.template($('#tpl_history').html()),
        todayTemplate: _.template($('#tpl_today').html()),
        createdTemplate: _.template($('#tpl_manage-events').html()),
        events: {
            "click #cal_tab ": "showCalendar",
            "click #req_tab ": "showRequests",
            "click .add-response": 'addResponse',
            "click #history_tab": "showHistory",
            "click .add-archive": "addArchive",
            "click #back_h": "showHistory",
            "click #created_events_tab": "showEvents",
            "click #raports_tab": "showReports",
            "click #received_tab": "showReceived",
            "click .x-img":"deleteEvent",
            'change #S_AR':"SendPClient",
            "click #send_p_c":"SendPhotClient"
        },

        initialize: function () {

            var router = this.options.router;
            var This = this;
            var globalId ;

            this.addArchive = function (event) {
                id = event.currentTarget.id;
                new_id = id.substr(2);
                console.log(new_id);
                this.globalId = new_id;
                $("#notifications").css("display", "none");
                $("#send_photos").css("display", "-webkit-box");
                $("#calevents").css("display", "none");
                $("#complete_requests").css("display", "none");
            };
            this.showEvents = function () {
                $("#notifications").css("display", "none");
                $("#send_photos").css("display", "none");
                $("#calevents").css("display", "none");
                $("#complete_requests").css("display", "none");
                $("#creadedevents").css("display", "-webkit-box");
                $("#req_tab").removeClass("active");
                $("#history_tab").removeClass("active");
                $("#cal_tab").removeClass("active");
                $("#created_events_tab").addClass("active");
                $("#raports_tab").removeClass("active");
                $("#reports").css("display", "none");

            };
            this.showReports = function () {
                $("#reports").css("display", "-webkit-box");
                $("#notifications").css("display", "none");
                $("#send_photos").css("display", "none");
                $("#calevents").css("display", "none");
                $("#complete_requests").css("display", "none");
                $("#creadedevents").css("display", "none");
                $("#req_tab").removeClass("active");
                $("#history_tab").removeClass("active");
                $("#cal_tab").removeClass("active");
                $("#created_events_tab").removeClass("active");
                $("#raports_tab").addClass("active");
            };
            this.showReceived = function () {
                $("#reports").css("display", "none");
                $("#notifications").css("display", "none");
                $("#send_photos").css("display", "none");
                $("#calevents").css("display", "none");
                $("#complete_requests").css("display", "none");
                $("#creadedevents").css("display", "none");
                $("#req_tab").removeClass("active");
                $("#history_tab").removeClass("active");
                $("#cal_tab").removeClass("active");
                $("#created_events_tab").removeClass("active");
                $("#raports_tab").removeClass("active");
                $("#received_photos").css("display", "-webkit-box");
                 $("#received_tab").addClass("active");
            };
            this.SendPClient = function(){
                inputPhotos = $('#S_AR').get(0).files;

                reader = new FileReader();
                reader.readAsDataURL(inputPhotos[0]);
                reader.onload = function (e) {
                    $('#iS_AR').attr("src","img/check.png");

                };
            };
            this.SendPhotClient = function(){
                zip = $('#S_AR').get(0).files;
                id = this.globalId;
                
                var formData = new FormData();
                formData.append('id', id);
                formData.append('zip', zip);
                formData.append('type', "contract");
                authenticationToken = router.getCookie("authenticated");
                var url = API_DOMAIN + 'contract_photos';
                $.ajax({
                    url: url,
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    mimeType: "multipart/form-data",
                    data: formData,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                        console.log('All good');

                    },
                    error: function (data) {
                        console.log('error');
                    }
                });


            };
            this.deleteEvent = function(event){
               var id = event.currentTarget.id;
                new_id = id.substr(2);
                authenticationToken = router.getCookie("authenticated");
                var url = API_DOMAIN + 'events?event_id='+new_id;
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    processData: false,
                    headers: { 'Authorization': authenticationToken },
                    success: function (data) {
                       This.model.loadCreatedEvents();

                    },
                    error: function (data) {
                        console.log('error');
                    }
                });
            };
            this.showCalendar = function () {
                $("#notifications").css("display", "none");
                $("#send_photos").css("display", "none");
                $("#calevents").css("display", "-webkit-box");
                $("#complete_requests").css("display", "none");
                $("#creadedevents").css("display", "none");
                $("#req_tab").removeClass("active");
                $("#history_tab").removeClass("active");
                $("#cal_tab").addClass("active");
                $('#calendar').fullCalendar('render');
                $("#created_events_tab").removeClass("active");
                $("#raports_tab").removeClass("active");
                $("#reports").css("display", "none");
            };

            this.showHistory = function () {
                $("#notifications").css("display", "none");
                $("#send_photos").css("display", "none");
                $("#calevents").css("display", "none");
                $("#complete_requests").css("display", "-webkit-box");
                $("#creadedevents").css("display", "none");
                $("#req_tab").removeClass("active");
                $("#cal_tab").removeClass("active");
                $("#history_tab").addClass("active");
                $("#created_events_tab").removeClass("active");
                $("#raports_tab").removeClass("active");
                $("#reports").css("display", "none");

            };
            this.showRequests = function () {
                $("#calevents").css("display", "none");
                $("#send_photos").css("display", "none");
                $("#notifications").css("display", "-webkit-box");
                $("#complete_requests").css("display", "none");
                $("#creadedevents").css("display", "none");
                $("#req_tab").addClass("active");
                $("#cal_tab").removeClass("active");
                $("#history_tab").removeClass("active");
                $("#created_events_tab").removeClass("active");
                $("#raports_tab").removeClass("active");
                $("#reports").css("display", "none");
            };
            this.addResponse = function (event) {
                var id = event.currentTarget.id;
                req_id = id.substr(2);
                var status = event.currentTarget.value;
                values = {
                    req_id: req_id,
                    status: status,
                }
                this.model.sendResponse(values);
            }
            addToday = function () {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth();
                var yyyy = today.getFullYear();
                var monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"];
                var day = $("#CALENDARP").find('#day-number');
                day.empty();
                day.append(dd);
                var month = $("#CALENDARP").find('#day-week');
                month.empty();
                month.append(monthNames[mm]);
                var year = $("#CALENDARP").find('#month-year');
                year.empty();
                year.append(yyyy);
            }
            loadChart = function (year,top) {
                function pad(d) {
                    return (d < 10) ? '0' + d : d;
                }
                function range(start, stop, step) {
                    var a = [pad(start)], b = start;
                    while (b < stop) { b += step; a.push(pad(b)) }
                    return a;
                }

                var oneBarChart = document.getElementById("one-bar-chart");
                if (oneBarChart !== null) {
                    var ctx_ob = oneBarChart.getContext("2d");
                    var data_ob = {
                        labels: range(1, 12, 1),
                        datasets: [
                            {
                                backgroundColor: "#38a9ff",
                                data: year
                            }]
                    };

                    var oneBarEl = new Chart(ctx_ob, {
                        type: 'bar',
                        data: data_ob,

                        options: {
                            deferred: {           // enabled by default
                                delay: 200        // delay of 500 ms after the canvas is considered inside the viewport
                            },
                            tooltips: {
                                enabled: false
                            },
                            legend: {
                                display: false
                            },
                            responsive: true,
                            scales: {
                                xAxes: [{
                                    stacked: false,
                                    barPercentage: 0.3,
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        fontColor: '#888da8'
                                    }
                                }],
                                yAxes: [{
                                    stacked: false,
                                    gridLines: {
                                        color: "#f0f4f9"
                                    },
                                    ticks: {
                                        beginAtZero: true,
                                        fontColor: '#888da8'
                                    }
                                }]
                            }
                        }
                    });
                }
                var pieColorChart = document.getElementById("pie-color-chart");

                if (pieColorChart !== null) {
                    var ctx_pc = pieColorChart.getContext("2d");
                    var data_pc = {
                        labels: top.label,
                        datasets: [
                            {
                                data: top.data,
                                borderWidth: 0,
                                backgroundColor: [
                                    "#7c5ac2",
                                    "#08ddc1",
                                    "#ff5e3a",
                                    "#ffd71b"
                                ]
                            }]
                    };

                    var pieColorEl = new Chart(ctx_pc, {
                        type: 'doughnut',
                        data: data_pc,
                        options: {
                            deferred: {           // enabled by default
                                delay: 300        // delay of 500 ms after the canvas is considered inside the viewport
                            },
                            cutoutPercentage: 93,
                            legend: {
                                display: false
                            },
                            animation: {
                                animateScale: false
                            }
                        }
                    });
                }

                
            };

            var reflectChange_visible = function (model, visible) {
                if (visible) {
                    this.$el.show();
                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay,listWeek'
                        },
                        navLinks: true, // can click day/week names to navigate views
                        eventLimit: true, // allow "more" link when too many events

                    });
                    $('#calendar').fullCalendar('addEvent', event);
                    addToday();
                    
                } else {
                    this.$el.hide();
                }
            };
            var reflectChange_events = function (model, events) {
                var req = this.$el.find('#accordion-today');
                $('#accordion-today').empty();
                var today = new Date();
                console.log('today', today);
                year = today.getFullYear();
                month = today.getMonth() + 1;
                day = today.getDate();
                if (month < 10) {
                    sm = '0' + month;
                }
                else {
                    sm = month;
                }
                for (e in events) {
                    n_year = events[e].start_date.substring(0, 4);
                    n_month = events[e].start_date.substring(5, 7);
                    n_day = events[e].start_date.substring(8, 10);

                    if (day == n_day && n_year == year && n_month == sm) {
                        req.append(This.todayTemplate({
                            title: 'Meeting with ' + events[e].f_name,
                            time: events[e].start_date.substring(11, 16)
                        }));
                    }
                    item = {
                        title: 'Meeting with ' + events[e].f_name,
                        start: events[e].start_date,
                        end: events[e].end_date,
                        id: events[e].id,
                    }
                    $('#calendar').fullCalendar('renderEvent', item, true);
                }
            };
            var reflectChange_requests = function (model, requests) {
                var req = this.$el.find('#table_req');
                req.empty();
                events = requests;
                console.log(events);
                for (e in events) {
                    date = events[e].start_date.substring(0, 10);
                    start_time = events[e].start_date.substring(11, 16);
                    end_time = events[e].end_date.substring(11, 16);
                    req.append(This.reqTemplate({
                        req_id: events[e].req_id,
                        name: events[e].f_name,
                        link: events[e].link,
                        date: date,
                        time: start_time + ' - ' + end_time,
                        text: events[e].message,
                        location: events[e].location,
                    }))
                }
            };

            var reflectChange_history = function (model, history) {
                var req = this.$el.find('#table_history');
                req.empty();
                events = history;
                console.log(events);
                for (e in events) {
                    date = events[e].start_date.substring(0, 10);
                    start_time = events[e].start_date.substring(11, 16);
                    end_time = events[e].end_date.substring(11, 16);
                    req.append(This.historyTemplate({
                        req_id: events[e].req_id,
                        name: events[e].f_name,
                        link: events[e].link,
                        date: date,
                        deadline: start_time + ' - ' + end_time,
                        text: events[e].message,
                        location: events[e].location,
                    }))
                }
            };

            var reflectChange_createdEvents = function (model, createdEvents) {
                var req = this.$el.find(cr_events);
                req.empty()
                for (e in createdEvents) {
                    event = createdEvents[e];
                    console.log('ev',event);
                    percent = event.volunteers * 100 / event.max_number;
                    type = event.type;
                    if (type == 0) {
                        image = 'img/concert.png'
                    }
                    else if (type == 1) {
                        image = 'img/festival.png'
                    }
                    else if (type == 2) {
                        image = 'img/conference.png'
                    }
                    else {
                        image = 'img/sport.png'
                    }
                    req.append(This.createdTemplate({
                        id:event.id,
                        name: event.name,
                        image: image,
                        percent: percent + '%'
                    }))
                }
            };
            var reflectChange_top = function (model, top) {
                console.log(top);
                var req = this.$el.find(report_top);
                req.empty();
                 var stat_text = this.$el.find('#text_sugestions');
                stat_text.empty();
                if (top.comments.difference > 0) {
                    diff = '+' + top.comments.difference;
                }
                else
                    diff = top.comments.difference;
                req.append('<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">\
					<div class="ui-block"><div class="ui-block-content">\
							<ul class="statistics-list-count"><li><div class="points">\
										<span> Number of comments </span></div>\
									<div class="count-stat">'+ top.comments.number + '\
										<span class="indicator positive" id="ind_pos">'+ diff + '</span>\
									</div></li></ul></div></div></div>');
                if (top.comments.difference > 0) {
                    var st = this.$el.find(ind_pos);
                    st.addClass('positive')
                }
                else {
                    var st = this.$el.find(ind_pos);
                    st.addClass('negative')
                }
                if (top.bookings.difference > 0) {
                    diff = '+' + top.bookings.difference;
                }
                else
                    diff = top.bookings.difference;
                req.append('<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">\
					<div class="ui-block"><div class="ui-block-content">\
							<ul class="statistics-list-count"><li><div class="points">\
										<span>Bookings for this month</span>\
									</div><div class="count-stat">'+ top.bookings.number + '\
										<span class="indicator " id="dif_neg">'+ diff + '</span>\
									</div></li></ul></div></div></div>');
                if (top.bookings.difference > 0) {
                    var st = this.$el.find(dif_neg);
                    st.addClass('positive')
                }
                else {
                    var st = this.$el.find(dif_neg);
                    st.addClass('negative')
                }
                console.log('find',PTypes[top.type.kind]);
                if(PTypes[top.type.kind] == undefined)
                    book = 1;
                else 
                    book =top.type.kind ;
                req.append('<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">\
					<div class="ui-block"><div class="ui-block-content">\
							<ul class="statistics-list-count"><li><div class="points">\
										<span> Your most booked photography type this month:</span>\
									</div><div style="padding-top: 15px;font-size:large;"><i style="margin-right: 10px;color: #08ddc1;" class="fa '+PTypes[book].icon+'" aria-hidden="true"></i>'+PTypes[book].name+'\
									</div>\
                                    </li></ul></div></div></div>');
                req.append('<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">\
					<div class="ui-block"><div class="ui-block-content"><ul class="statistics-list-count">\
								<li><div class="points"><span>\
									Regional ranking</span></div>\
								<div style="display: -webkit-box;"><div class="count-stat">'+top.top +'</div><div class="author-thumb">\
			<img src="img/rank.png" alt="author" data-pin-nopin="true">\
		</div></div></li></ul></div></div></div>');

                loadChart(top.year,top.top_type);

                text_stats = Sugestions;
                for(s in text_stats){
                    stat_text.append('<div ><i style="margin-right: 10px;color: #08ddc1;font-size:large;" class="fa '+PTypes[text_stats[s].id].icon+'" aria-hidden="true"></i>'+PTypes[text_stats[s].id].name+'  '+text_stats[s].text+'</div><hr>');
                }                    


            };


            this.listenTo(this.model, 'change:visible', reflectChange_visible);
            this.listenTo(this.model, 'change:events', reflectChange_events);
            this.listenTo(this.model, 'change:requests', reflectChange_requests);
            this.listenTo(this.model, 'change:history', reflectChange_history);
            this.listenTo(this.model, 'change:top', reflectChange_top);
            this.listenTo(this.model, 'change:createdEvents', reflectChange_createdEvents);
        },

    });

});