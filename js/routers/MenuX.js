define([
    'models/MenuPaneM',
    'views/MenuPaneV'
], function (MenuPaneModel, MenuPaneView) {
    return {

        menu_initialize: function () {
            this.menuPaneModel = new MenuPaneModel();
            new MenuPaneView({
                model: this.menuPaneModel,
                router: this
            });
        },

        menuPane_resolveAuthentication: function() {
            this.menuPaneModel.resolveAuthentication(
                this.isAuthenticated()
            );
        }

    };
});