define([
    'models/CreateEventPaneM',
    'views/CreateEventPaneV'
], function(
    CreateEventPaneModel,
    CreateEventPaneView
) {
    return {

        createEvent_initialize: function() {
            this.createEventPaneModel = new CreateEventPaneModel({
                router: this
            });
            new CreateEventPaneView({
                model: this.createEventPaneModel,
                router: this
            });
        },

      createEvent_enter: function() {
            this.parentPane_toggle(SECTIONS.createEvent);
        }

    };
});