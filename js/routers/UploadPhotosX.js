define([
    'models/UploadPhotosPaneM',
    'views/UploadPhotosPaneV'
], function(
    UploadPhotosPaneModel,
    UploadPhotosPaneView
) {
    return {

        uploadPhotos_initialize: function() {
            this.uploadPhotosPaneModel = new UploadPhotosPaneModel({
                router: this
            });
            new UploadPhotosPaneView({
                model: this.uploadPhotosPaneModel,
                router: this
            });
        },

      uploadPhotos_enter: function() {
            this.parentPane_toggle(SECTIONS.uploadPhotos);
        }

    };
});