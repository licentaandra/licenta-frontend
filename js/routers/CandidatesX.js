define([
    'models/CandidatesPaneM',
    'views/CandidatesPaneV'
], function(
    CandidatesPaneModel,
    CandidatesPaneView
) {
    return {

        candidates_initialize: function() {
            this.candidatesPaneModel = new CandidatesPaneModel({
                router: this
            });
            new CandidatesPaneView({
                model: this.candidatesPaneModel,
                router: this
            });
        },

    //    candidates_enter: function() {
    //         this.parentPane_toggle(SECTIONS.candidates);
    //     }

    candidates_reflectPath_selectedTab: function(segments) {
            this.path.setSelectedTab(segments.selectedTab);
            this.path.updateUrl();
        }
        

    };
});