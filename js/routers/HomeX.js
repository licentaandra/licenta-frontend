define([
    'models/HomePaneM',
    'views/HomePaneV'
], function(
    HomePaneModel,
    HomePaneView
) {
    return {

        home_initialize: function() {
            this.homePaneModel = new HomePaneModel({
                router: this
            });
            new HomePaneView({
                model: this.homePaneModel,
                router: this
            });
        },

      home_enter: function() {
            this.parentPane_toggle(SECTIONS.home);
        }

    };
});