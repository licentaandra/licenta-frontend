define([
    'models/ProfilePaneM',
    'views/ProfilePaneV'
], function(
    ProfilePaneModel,
    ProfilePaneView
) {
    return {

        profile_initialize: function() {
            this.profilePaneModel = new ProfilePaneModel({
                router: this
            });
            new ProfilePaneView({
                model: this.profilePaneModel,
                router: this
            });
        },

      profile_enter: function(id) {
            // this.path.setID(id);
            // this.path.updateUrl();
            this.profilePaneModel.setId(id);
            this.parentPane_toggle(SECTIONS.profile);
            this.path.setID(id);
            this.path.updateUrl();
        }
    };
});