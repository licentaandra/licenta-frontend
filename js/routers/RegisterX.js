define([
    'models/RegisterPaneM',
    'views/RegisterPaneV'
], function(
    RegisterPaneModel,
    RegisterPaneView
) {
    return {

        register_initialize: function() {
            this.registerPaneModel = new RegisterPaneModel({
                router: this
            });
            new RegisterPaneView({
                model: this.registerPaneModel,
                router: this
            });
        },

      register_enter: function() {
            this.parentPane_toggle(SECTIONS.register);
        }

    };
});