define([
    'models/EventsPaneM',
    'views/EventsPaneV'
], function(
    EventsPaneModel,
    EventsPaneView
) {
    return {

        events_initialize: function() {
            this.eventsPaneModel = new EventsPaneModel({
                router: this
            });
            new EventsPaneView({
                model: this.eventsPaneModel,
                router: this
            });
        },

      events_enter: function() {
            this.parentPane_toggle(SECTIONS.events);
        }

    };
});