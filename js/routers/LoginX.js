define([
    'models/LoginPaneM',
    'views/LoginPaneV'
], function (
    LoginPaneModel,
    LoginPaneView
) {
        return {

            login_initialize: function () {
                this.loginPaneModel = new LoginPaneModel({
                    router: this
                });
                new LoginPaneView({
                    model: this.loginPaneModel,
                    router: this
                });
            },
            getCookie: function (cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return null;
            },
            setCookie: function (cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            },


            login_enter: function () {
                this.parentPane_toggle(SECTIONS.login);
            }


        };
    });