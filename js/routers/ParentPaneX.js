define([
    'models/ParentPaneM',
    'views/ParentPaneV'
], function (
    ParentPaneModel,
    ParentPaneView
) {

        return {

            parentPane_initialize: function () {

                this.parentPaneModel = new ParentPaneModel();
                new ParentPaneView({
                    model: this.parentPaneModel
                });

                this.parentPane_toggle = function (pane) {
                    switch (pane) {

                        case SECTIONS.login:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.uploadPhotosPaneModel.show(false);
                            this.loginPaneModel.show(true);
                            this.detailsPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;

                        case SECTIONS.featured:
                            // this.menuPaneModel.setSelectedItem(pane);
                            // this.menuPane_resolveAuthentication();

                            this.uploadPhotosPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.detailsPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(true);


                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;

                        case SECTIONS.profile:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.uploadPhotosPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.detailsPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.profilePaneModel.show(true);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;

                        case SECTIONS.uploadPhotos:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.uploadPhotosPaneModel.show(true);
                            this.loginPaneModel.show(false);
                            this.detailsPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;
                        case SECTIONS.candidates:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();


                            this.candidatesPaneModel.show(true);
                            this.loginPaneModel.show(false);
                            this.detailsPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;

                        case SECTIONS.details:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.detailsPaneModel.show(true);
                            this.candidatesPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);


                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;

                        case SECTIONS.forgot:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.forgotPaneModel.show(true);
                            this.detailsPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;
                        case SECTIONS.register:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.detailsPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.registerPaneModel.show(true);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;


                        case SECTIONS.account_details:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.detailsPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(true);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;

                        case SECTIONS.home:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.detailsPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(true);
                            this.calendarPaneModel.show(false);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;

                        case SECTIONS.calendar:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.detailsPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(true);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.eventsPaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);


                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;

                        case SECTIONS.events:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.eventsPaneModel.show(true);
                            this.detailsPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.createEventPaneModel.show(false);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;
                        case SECTIONS.createEvent:
                            this.menuPaneModel.setSelectedItem(pane);
                            this.menuPane_resolveAuthentication();

                            this.eventsPaneModel.show(false);
                            this.detailsPaneModel.show(false);
                            this.forgotPaneModel.show(false);
                            this.candidatesPaneModel.show(false);
                            this.loginPaneModel.show(false);
                            this.registerPaneModel.show(false);
                            this.accountDetailsPaneModel.show(false);
                            this.homePaneModel.show(false);
                            this.calendarPaneModel.show(false);
                            this.uploadPhotosPaneModel.show(false);
                            this.profilePaneModel.show(false);
                            this.createEventPaneModel.show(true);
                            this.featuredPaneModel.show(false);

                            this.path.updateSection(pane);
                            this.path.updateUrl();
                            break;
                    }
                };
            }

        };
    });