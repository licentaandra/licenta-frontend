define([
    'models/AccountDetailsPaneM',
    'views/AccountDetailsPaneV'
], function(
    AccountDetailsPaneModel,
    AccountDetailsPaneView
) {
    return {

        accountDetails_initialize: function() {
            this.accountDetailsPaneModel = new AccountDetailsPaneModel({
                router: this
            });
            new AccountDetailsPaneView({
                model: this.accountDetailsPaneModel,
                router: this
            });
        },

     accountDetails_enter: function() {
            role = this.getRole();
            this.parentPane_toggle(SECTIONS.account_details);
        }

    };
});