define([
    'models/ForgotPaneM',
    'views/ForgotPaneV'
], function(
    ForgotPaneModel,
    ForgotPaneView
) {
    return {

        forgot_initialize: function() {
            this.forgotPaneModel = new ForgotPaneModel({
                router: this
            });
            new ForgotPaneView({
                model: this.forgotPaneModel,
                router: this
            });
        },

      forgot_enter: function() {
            this.parentPane_toggle(SECTIONS.forgot);
        }

    };
});