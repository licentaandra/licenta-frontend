define([
    'models/FeaturedPaneM',
    'views/FeaturedPaneV'
], function(
    FeaturedPaneModel,
    FeaturedPaneView
) {
    return {

        featured_initialize: function() {
            this.featuredPaneModel = new FeaturedPaneModel({
                router: this
            });
            new FeaturedPaneView({
                model: this.featuredPaneModel,
                router: this
            });
        },

      featured_enter: function() {
            this.parentPane_toggle(SECTIONS.featured);
        }

    };
});