define([
    'models/DetailsPaneM',
    'views/DetailsPaneV'
], function(
    DetailsPaneModel,
    DetailsPaneView
) {
    return {

        details_initialize: function() {
            this.detailsPaneModel = new DetailsPaneModel({
                router: this
            });
            new DetailsPaneView({
                model: this.detailsPaneModel,
                router: this
            });
        },

      details_enter: function(candidateID) {
            this.detailsPaneModel.setCandidateID(candidateID);
            this.parentPane_toggle(SECTIONS.details);
        }

    };
});