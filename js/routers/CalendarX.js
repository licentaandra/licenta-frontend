define([
    'models/CalendarPaneM',
    'views/CalendarPaneV'
], function(
    CalendarPaneModel,
    CalendarPaneView
) {
    return {

        calendar_initialize: function() {
            this.calendarPaneModel = new CalendarPaneModel({
                router: this
            });
            new CalendarPaneView({
                model: this.calendarPaneModel,
                router: this
            });
        },

      calendar_enter: function() {
            this.parentPane_toggle(SECTIONS.calendar);
        }

    };
});